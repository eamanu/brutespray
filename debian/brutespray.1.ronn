brutespray(8) -- Python bruteforce tool
====

## SYNOPSIS

`brutespray` [Usage]: brutespray [-h] -f FILE [-o OUTPUT] [-s SERVICE] [-t THREADS] [-T HOSTS] [-U USERLIST] [-P PASSLIST] [-u USERNAME] [-p PASSWORD] [-c] [-i].  


## DESCRIPTION


BruteSpray takes nmap GNMAP/XML output and automatically brute-forces services  
with default credentials using Medusa. 
BruteSpray can even find non-standard ports by using the -sV inside Nmap.


   * `-f` FILE, `--file` FILE  
    GNMAP or XML file to parse  

   * `-o` OUTPUT, `--output` OUTPUT  
    Directory containing successful attempts  

   * `-s` SERVICE, `--service` SERVICE  
    Specify service to attack  
  
   * `-t` THREADS, `--threads` THREADS  
    Number of medusa threads  
 
   * `-T` HOSTS, `--hosts` HOSTS  
    Number of hosts to test concurrently  

   * `-U` USERLIST, `--userlist` USERLIST  
    Reference a custom username file  

   * `-P` PASSLIST, `--passlist` PASSLIST  
    Reference a custom password file  

   * `-u` USERNAME, `--username` USERNAME  
    Specify a single username  

   * `-p` PASSWORD, `--password` PASSWORD  
    Specify a single password  

   * `-c`, `--continuous`  
    Keep brute-forcing after success  

   * `-i`, `--interactive`  
    Interactive mode  


Commands: 

* brutespray -h
* brutespray --file nmap.gnmap
* brutespray --file nmap.xml
* brutespray --file nmap.xml -i


## EXAMPLES

* Nmap network scanning example:  
nmap -sS -sV 192.168.0.0/24 -vv -n -oA mynetwork

* Using Custom Wordlists:  
brutespray --file mynetwork.gnmap -U /usr/share/brutespray/wordlist/user.txt -P /usr/share/brutespray/wordlist/pass.txt --threads 5 --hosts 5

* Brute-Forcing Specific Services:  
brutespray --file mynetwork.gnmap --service ftp,ssh,telnet --threads 5 --hosts 5

* Specific Credentials:  
brutespray --file mynetwork.gnmap -u admin -p password --threads 5 --hosts 5

* Continue After Success:  
brutespray --file mynetwork.gnmap --threads 5 --hosts 5 -c

* Use Nmap XML Output:  
brutespray --file mynetwork.xml --threads 5 --hosts 5

* Interactive Mode:  
brutespray --file mynetwork.xml -i


## SEE ALSO
-  [ On github](https://github.com/x90skysn3k/brutespray)
-  nmap(1)

## MANPAGE AUTHOR

Stephane Neveu <stefneveu@gmail.com>
